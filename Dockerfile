FROM node:18.1-alpine3.14 as builder
WORKDIR /app
COPY package.json yarn.lock ./
RUN corepack enable
RUN yarn install --production --frozen-lockfile --check-cache --immutable --immutable-cache 
COPY . .
ENV NODE_ENV=production
COPY .env.$NODE_ENV .env
RUN yarn build


FROM node:18.1.0-alpine as production
WORKDIR /usr/app/
COPY --from=builder /app/dist ./build
COPY package.json yarn.lock ./
ENV NODE_ENV=production
COPY .env.$NODE_ENV .env
RUN yarn install --production
USER node
EXPOSE 3000
CMD ["node", "build/main"]
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { getEnvPath } from './common/helper/env.helper';
import { ConfigModule } from '@nestjs/config';
import { ModulesModule } from './modules/modules.module';
import { UsersModule } from './users/users.module';
import { ExercisesModule } from './exercises/exercises.module';
import { TokensModule } from './tokens/tokens.module';
import { AuthModule } from './auth/auth.module';
import { SubmissionModule } from './submissions/submissions.module';
import { LanguagesService } from './languages/languages.service';
import { MailModule } from './mail/mail.module';
import { ValidationsService } from './validations/validations.service';
import { ValidationsModule } from './validations/validations.module';

const envFilePath: string = getEnvPath(`${__dirname}`);

@Module({
  imports: [
    ConfigModule.forRoot({ envFilePath, isGlobal: true }),
    ExercisesModule,
    UsersModule,
    SubmissionModule,
    ModulesModule,
    TokensModule,
    AuthModule,
    MailModule,
    ValidationsModule,
  ],
  controllers: [AppController],
  providers: [AppService, LanguagesService, ValidationsService],
})
export class AppModule {}

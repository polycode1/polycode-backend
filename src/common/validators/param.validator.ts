import { IsBooleanString, IsNumberString, IsUUID } from 'class-validator';

export class FindOneId {
  @IsNumberString()
  id: number;
}

export class FindOneUuid {
  @IsUUID()
  uuid: string;
}

export class FindQueryUserId {
  @IsNumberString()
  userId: number;
}

export class FindQueryShowSolution {
  @IsBooleanString()
  showSolution: string;
}

export const USERS_REPOSITORY = 'USERS_REPOSITORY';
export const EXERCISES_REPOSITORY = 'EXERCISES_REPOSITORY';
export const MODULES_REPOSITORY = 'MODULES_REPOSITORY';
export const SUBMISSIONS_REPOSITORY = 'SUBMISSIONS_REPOSITORY';
export const TOKENS_REPOSITORY = 'TOKENS_REPOSITORY';
export const LANGUAGES_REPOSITORY = 'LANGUAGES_REPOSITORY';
export const VALIDATIONS_REPOSITORY = 'VALIDATIONS_REPOSITORY';
export const ORM = 'SEQUELIZE';
export enum LANGUAGE {
  JAVASCRIPT = 'javascript',
  JAVA = 'java',
  RUST = 'rust',
  PYTHON = 'python',
}
export enum ExerciseStatus {
  STARTED = 'started',
  FINISHED = 'finished',
}
export enum ModuleType {
  PRACTICE = 'practice',
  CHALLENGE = 'challenge',
}

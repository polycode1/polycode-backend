import { Sequelize } from 'sequelize-typescript';
import { Exercise } from 'src/exercises/entities/exercise.entity';
import { Language } from 'src/languages/entities/language.entity';
import { Module } from 'src/modules/entities/module.entity';
import { Submission } from 'src/submissions/entities/submission.entity';
import { Token } from 'src/tokens/entities/token.entities';
import { Validation } from 'src/validations/entities/validation.entity';
import { ORM } from '../constants';
import { User } from '../users/entities/user.entity';

export const databaseProviders = [
  {
    provide: ORM,
    useFactory: async () => {
      const sequelize = new Sequelize(process.env.DB_URL, {
        dialect: 'postgres',
        logging: false,
      });
      sequelize.addModels([
        User,
        Module,
        Exercise,
        Token,
        Submission,
        Language,
        Validation,
      ]);
      await sequelize.sync();
      return sequelize;
    },
  },
];

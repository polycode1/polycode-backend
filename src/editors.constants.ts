export const RUST_DEFAULT_EDITOR = `
fn main() {
  //Code goes here
  println!("Hello world!");
}
`;

export const PYTHON_DEFAULT_EDITOR = `
def main():
  //Code goes here
  print("Hello world!")

main()
`;

export const JAVA_DEFAULT_EDITOR = `
public class Main {
  public static void main(String[] args) {
      //Code goes here
    System.out.println("Hello World!");
  }
}
`;

export const JAVASCRIPT_DEFAULT_EDITOR = `
function main() {
  console.log("Hello World!");
}

main()
`;

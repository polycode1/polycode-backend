import { IsNotEmpty, ValidateNested } from 'class-validator';
import { Language } from 'src/languages/entities/language.entity';

export class CreateExerciseDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  statement: string;

  @ValidateNested({ each: true })
  languages: Language[];

  @IsNotEmpty()
  solution: string;
}

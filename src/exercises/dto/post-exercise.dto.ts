import { ExerciseStatus } from 'src/constants';

//Réponse du back à une submission d'Exercise
export class PostExercise {
  status: ExerciseStatus;
  stdout: string;
  stderr: string;
}

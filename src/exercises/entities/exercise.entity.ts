import { Module } from '../../modules/entities/module.entity';
import {
  BelongsTo,
  BelongsToMany,
  Column,
  Table,
  Model,
  ForeignKey,
  Unique,
  HasMany,
  DataType,
} from 'sequelize-typescript';
import { User } from 'src/users/entities/user.entity';
import { Submission } from 'src/submissions/entities/submission.entity';
import { Language } from 'src/languages/entities/language.entity';

@Table
export class Exercise extends Model<Exercise> {
  @Unique('Exercise name must be unique')
  @Column
  name: string;

  @Column(DataType.TEXT)
  statement: string;

  @HasMany(() => Language, { onUpdate: 'CASCADE', onDelete: 'CASCADE' })
  languages: Language[];

  @Column
  solution: string;

  @ForeignKey(() => Module)
  @Column
  moduleId: number;

  @BelongsToMany(() => User, () => Submission)
  users: User[];

  @HasMany(() => Submission)
  submissions: Submission[];

  @BelongsTo(() => Module, { as: 'module', foreignKey: 'moduleId' })
  module: Module;
}

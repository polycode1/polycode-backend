import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpException,
  HttpStatus,
  Query,
  UseGuards,
  Request,
} from '@nestjs/common';
import { ExercisesService } from './exercises.service';
import { CreateExerciseDto } from './dto/create-exercise.dto';
import { UpdateExerciseDto } from './dto/update-exercise.dto';
import { FindQueryShowSolution } from 'src/common/validators/param.validator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';

@Controller('exercises')
export class ExercisesController {
  constructor(private readonly exercisesService: ExercisesService) {}

  @Post()
  async create(@Body() createExerciseDto: CreateExerciseDto) {
    if (await this.exercisesService.findByName(createExerciseDto.name)) {
      throw new HttpException(
        'Exercise name already exists',
        HttpStatus.CONFLICT,
      );
    } else {
      return this.exercisesService.create(createExerciseDto);
    }
  }

  @Get()
  findAll() {
    return this.exercisesService.findAll();
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async findOne(
    @Param('id') id: string,
    @Query() query: FindQueryShowSolution,
    @Request() req,
  ) {
    const exercise = await this.exercisesService.findOneByUserId(
      +id,
      req.user.id,
      query.showSolution === 'true',
    );
    if (exercise) {
      return exercise;
    } else {
      throw new HttpException('Exercise not found', HttpStatus.NOT_FOUND);
    }
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateExerciseDto: UpdateExerciseDto,
  ) {
    return this.exercisesService.update(+id, updateExerciseDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    if (this.exercisesService.remove(+id)) {
      throw new HttpException('', HttpStatus.NO_CONTENT);
    } else {
      throw new HttpException('Exercise not found', HttpStatus.NOT_FOUND);
    }
  }
}

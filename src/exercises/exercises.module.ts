import { Module } from '@nestjs/common';
import { ExercisesService } from './exercises.service';
import { ExercisesController } from './exercises.controller';
import { DatabaseModule } from 'src/database/database.module';
import { exercisesProviders } from './exercises.providers';

@Module({
  imports: [DatabaseModule],
  controllers: [ExercisesController],
  providers: [ExercisesService, ...exercisesProviders],
  exports: [ExercisesService],
})
export class ExercisesModule {}

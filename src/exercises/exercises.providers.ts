import { EXERCISES_REPOSITORY } from '../constants';
import { Exercise } from './entities/exercise.entity';

export const exercisesProviders = [
  {
    provide: EXERCISES_REPOSITORY,
    useValue: Exercise,
  },
];

import { Inject, Injectable } from '@nestjs/common';
import { EXERCISES_REPOSITORY } from 'src/constants';
import { Language } from 'src/languages/entities/language.entity';
import { Submission } from 'src/submissions/entities/submission.entity';
import { CreateExerciseDto } from './dto/create-exercise.dto';
import { UpdateExerciseDto } from './dto/update-exercise.dto';
import { Exercise } from './entities/exercise.entity';

@Injectable()
export class ExercisesService {
  constructor(
    @Inject(EXERCISES_REPOSITORY)
    private exerciseRepository: typeof Exercise,
  ) {}

  async create(createExerciseDto: CreateExerciseDto) {
    return (
      await this.exerciseRepository.create(
        { ...createExerciseDto },
        { raw: true, include: [Language] },
      )
    ).toJSON();
  }

  findAll() {
    return this.exerciseRepository.findAll<Exercise>();
  }

  findOne(id: number, showSolution = false) {
    return this.exerciseRepository.findOne<Exercise>({
      where: { id },
      attributes: {
        exclude: showSolution ? [] : ['solution'],
      },
      raw: true,
    });
  }

  findOneByUserId(id: number, userId: number, showSolution = false) {
    return this.exerciseRepository.findOne<Exercise>({
      where: { id },
      attributes: {
        exclude: showSolution ? [] : ['solution'],
      },
      include: Submission,
      raw: true,
    });
  }

  findByName(name: string) {
    return this.exerciseRepository.findOne<Exercise>({
      where: { name: name },
      raw: true,
    });
  }

  async update(id: number, updateExerciseDto: UpdateExerciseDto) {
    const response = await this.exerciseRepository.update(updateExerciseDto, {
      where: { id },
      returning: true,
    });
    if (response[0] === 1) {
      return response[1][0].toJSON();
    } else {
      return 0;
    }
  }

  async remove(id: number) {
    return 1 === (await this.exerciseRepository.destroy({ where: { id } }));
  }
}

import { IsNotEmpty } from 'class-validator';
import { LANGUAGE } from 'src/constants';

export class CreateLanguageDto {
  @IsNotEmpty()
  name: LANGUAGE;
}

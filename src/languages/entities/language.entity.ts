import { ENUM } from 'sequelize';
import { Column, ForeignKey, Model, Table } from 'sequelize-typescript';
import { LANGUAGE } from 'src/constants';
import { Exercise } from 'src/exercises/entities/exercise.entity';
import { Module } from 'src/modules/entities/module.entity';

@Table
export class Language extends Model {
  @Column(
    ENUM(LANGUAGE.JAVASCRIPT, LANGUAGE.JAVA, LANGUAGE.PYTHON, LANGUAGE.RUST),
  )
  value: string;

  @ForeignKey(() => Module)
  @Column
  moduleId: number;

  @ForeignKey(() => Exercise)
  @Column
  exerciseId: number;
}

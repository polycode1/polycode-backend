import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {}

  async sendUserConfirmation(user: User, token: string) {
    const url = `${process.env.MAIL_REDIRECT_URL}${token}`;
    const year = new Date().getFullYear();

    await this.mailerService.sendMail({
      to: user.email,
      subject: 'Welcome to PolyCode ⌨️ ! Confirm your Email',
      template: './confirmation',
      html: `<h1>Hello ${user.username},</h1>
      <p> Please click below to confirm your email</p>
      <p>
          <button>
            <a href="${url}" target="_blank">Confirm</a>
          </button>
      </p>
      
      <p>If you did not request this email you can safely ignore it.</p>
      
      <small><i>PolyCode ${year} - Arsène Fougerouse</i></small`,
      // context: {
      //   name: user.username,
      //   url,
      // },
    });
  }
}

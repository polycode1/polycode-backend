import { IsEnum, IsNotEmpty, Length, ValidateNested } from 'class-validator';
import { ModuleType } from 'src/constants';
import { Exercise } from 'src/exercises/entities/exercise.entity';
import { Language } from 'src/languages/entities/language.entity';

export class CreateModuleDto {
  @IsNotEmpty()
  name: string;

  @Length(20, 140)
  description: string;

  @IsNotEmpty()
  @IsEnum(ModuleType)
  type: ModuleType;

  @ValidateNested({ each: true })
  exercises: Exercise[];

  @ValidateNested({ each: true })
  languages: Language[];
}

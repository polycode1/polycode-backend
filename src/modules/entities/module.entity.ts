import { ArrayNotEmpty, IsNotEmpty, Length } from 'class-validator';
import {
  Column,
  HasMany,
  Index,
  Table,
  Unique,
  Model,
} from 'sequelize-typescript';
import { ModuleType } from 'src/constants';
import { Exercise } from 'src/exercises/entities/exercise.entity';
import { Language } from 'src/languages/entities/language.entity';

@Table
export class Module extends Model<Module> {
  @Index({ unique: true })
  @Unique
  @Column
  name: string;

  @Length(20, 140)
  @Column
  description: string;

  @Column
  @IsNotEmpty()
  type: ModuleType;

  @ArrayNotEmpty()
  @HasMany(() => Language, { onUpdate: 'CASCADE', onDelete: 'CASCADE' })
  languages: Language[];

  @HasMany(() => Exercise, { onUpdate: 'CASCADE', onDelete: 'CASCADE' })
  exercises: Exercise[];
}

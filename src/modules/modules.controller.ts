import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ValidationPipe,
  UsePipes,
  HttpException,
  HttpStatus,
  UseGuards,
  Query,
  Request,
} from '@nestjs/common';
import { ModulesService } from './modules.service';
import { CreateModuleDto } from './dto/create-module.dto';
import { UpdateModuleDto } from './dto/update-module.dto';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { FindQueryUserId } from 'src/common/validators/param.validator';

@Controller('modules')
export class ModulesController {
  constructor(private readonly modulesService: ModulesService) {}

  @Post()
  @UsePipes(new ValidationPipe({ transform: true }))
  async create(
    @Body() createModuleDto: CreateModuleDto,
  ): Promise<CreateModuleDto> {
    if (await this.modulesService.findOneByName(createModuleDto.name)) {
      throw new HttpException(
        'Module name already exists',
        HttpStatus.CONFLICT,
      );
    } else {
      return this.modulesService.create(createModuleDto);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  @UsePipes(new ValidationPipe({ transform: true }))
  findAll(@Query() query, @Request() req) {
    if (query.type && query.trending) {
      return this.modulesService.findTrending(query.type);
    } else if (query.type) {
      return this.modulesService.findAllByType(query.type);
    } else {
      return this.modulesService.findAllUserId(req.user.id);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get('/users/:userId')
  findAllForUser(
    @Param() params: FindQueryUserId,
    @Request() req,
    @Query() query,
  ) {
    if (req.user.id !== +params.userId) {
      throw new HttpException(
        `You don't have the right to access this userId`,
        HttpStatus.FORBIDDEN,
      );
    } else if (query.type) {
      return this.modulesService.findAllByTypeAndUserId(
        query.type,
        +params.userId,
      );
    } else {
      return this.modulesService.findAllByUserId(+params.userId);
    }
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.modulesService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateModuleDto: UpdateModuleDto) {
    return this.modulesService.update(+id, updateModuleDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    if (await this.modulesService.remove(+id)) {
      throw new HttpException('', HttpStatus.NO_CONTENT);
    }
    return this.modulesService.remove(+id);
  }
}

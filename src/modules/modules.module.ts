import { Module } from '@nestjs/common';
import { ModulesService } from './modules.service';
import { ModulesController } from './modules.controller';
import { DatabaseModule } from 'src/database/database.module';
import { modulesProviders } from './modules.providers';

@Module({
  imports: [DatabaseModule],
  controllers: [ModulesController],
  providers: [ModulesService, ...modulesProviders],
})
export class ModulesModule {}

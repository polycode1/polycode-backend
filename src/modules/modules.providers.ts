import { MODULES_REPOSITORY } from 'src/constants';
import { Module } from './entities/module.entity';

export const modulesProviders = [
  {
    provide: MODULES_REPOSITORY,
    useValue: Module,
  },
];

import { Inject, Injectable } from '@nestjs/common';
import { ModuleType } from 'src/constants';
import { Exercise } from 'src/exercises/entities/exercise.entity';
import { Language } from 'src/languages/entities/language.entity';
import { Submission } from 'src/submissions/entities/submission.entity';
import { CreateModuleDto } from './dto/create-module.dto';
import { UpdateModuleDto } from './dto/update-module.dto';
import { Module } from './entities/module.entity';

@Injectable()
export class ModulesService {
  constructor(
    @Inject('MODULES_REPOSITORY')
    private moduleRepository: typeof Module,
  ) {}

  create(createModuleDto: CreateModuleDto) {
    return this.moduleRepository.create(
      { ...createModuleDto },
      {
        returning: true,
        raw: true,
        include: [Exercise, Language],
      },
    );
  }

  findAllByUserId(userId: number) {
    return this.moduleRepository.findAll<Module>({
      include: [
        {
          model: Exercise,
          attributes: { exclude: ['solution'] },
          include: [{ model: Submission, where: { userId: userId } }],
        },
        Language,
      ],
    });
  }

  findAllByType(type: ModuleType) {
    return this.moduleRepository.findAll<Module>({
      where: { type: type },
      include: [
        { model: Exercise, attributes: { exclude: ['solution'] } },
        Language,
      ],
    });
  }

  async findTrending(type: ModuleType) {
    const name = type === 'challenge' ? 'Journey to Mars' : 'Basis of coding';
    return this.moduleRepository.findAll<Module>({
      where: { type: type, name: name },
      include: [
        { model: Exercise, attributes: { exclude: ['solution'] } },
        Language,
      ],
    });
  }

  async findAllByTypeAndUserId(type: ModuleType, userId: number) {
    const modules = await this.moduleRepository.findAll<Module>({
      where: { type: type, '$exercises.submissions.userId$': userId },
      include: [
        {
          model: Exercise,
          attributes: { exclude: ['solution'] },
          include: [{ model: Submission, where: { userId: userId } }],
        },
        Language,
      ],
      raw: true,
    });
    // console.log(modules);
    // modules.forEach(module => {
    //   console.log(module.exercises.length)
    // });

    return modules;
    return modules.filter((module) => module.toJSON().exercises.length < 1);
  }

  findAllUserId(userId: number) {
    return this.moduleRepository.findAll<Module>({
      include: [
        {
          model: Exercise,
          attributes: { exclude: ['solution'] },
          include: [
            { model: Submission, where: { userId: userId }, required: false },
            Language,
          ],
        },
        Language,
      ],
    });
  }

  findOne(id: number) {
    return this.moduleRepository.findOne<Module>({
      where: { id },
      include: [
        { model: Exercise, attributes: { exclude: ['solution'] } },
        Language,
      ],
    });
  }

  findOneByName(name: string): Promise<Module> {
    return this.moduleRepository.findOne({ where: { name }, raw: true });
  }

  async update(id: number, updateModuleDto: UpdateModuleDto) {
    const response = await this.moduleRepository.update(updateModuleDto, {
      where: { id },
      returning: true,
    });
    if (response[0] === 1) {
      return response[1][0].toJSON();
    } else {
      return 0;
    }
  }

  async remove(id: number) {
    return 1 === (await this.moduleRepository.destroy({ where: { id } }));
  }
}

import { IsEnum, IsNotEmpty, IsNumber } from 'class-validator';
import { LANGUAGE } from 'src/constants';

export class CreateSubmissionDto {
  @IsNotEmpty()
  @IsNumber()
  exerciseId: number;

  @IsNotEmpty()
  @IsNumber()
  userId: number;

  @IsNotEmpty()
  code: string;

  @IsNotEmpty()
  @IsEnum(LANGUAGE)
  language: LANGUAGE;
}

import { IsNotEmpty } from 'class-validator';
import { ExerciseStatus } from 'src/constants';

export class PostSubmissionDto {
  @IsNotEmpty()
  statusCode: number;

  @IsNotEmpty()
  stdout: number;

  @IsNotEmpty()
  stderr: string;

  @IsNotEmpty()
  status: ExerciseStatus;
}

import {
  Column,
  Table,
  Model,
  ForeignKey,
  DataType,
  BelongsTo,
  Default,
} from 'sequelize-typescript';
import { ExerciseStatus, LANGUAGE } from 'src/constants';
import { Exercise as Exercise } from 'src/exercises/entities/exercise.entity';
import { User } from 'src/users/entities/user.entity';

@Table
export class Submission extends Model<Submission> {
  @ForeignKey(() => Exercise)
  @Column
  exerciseId: number;

  @ForeignKey(() => User)
  @Column
  userId: number;

  @BelongsTo(() => User)
  user: User;

  @BelongsTo(() => Exercise)
  exercise: Exercise;

  @Default(ExerciseStatus.STARTED)
  @Column(DataType.ENUM(ExerciseStatus.FINISHED, ExerciseStatus.STARTED))
  status: ExerciseStatus;

  @Column(
    DataType.ENUM(
      LANGUAGE.JAVASCRIPT,
      LANGUAGE.JAVA,
      LANGUAGE.PYTHON,
      LANGUAGE.RUST,
    ),
  )
  language: LANGUAGE;

  @Column
  code: string;
}

import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ValidationPipe,
  UsePipes,
  HttpStatus,
  HttpException,
  UseGuards,
  Request,
} from '@nestjs/common';
import { SubmissionsService } from './submissions.service';
import { CreateSubmissionDto } from './dto/create-submission.dto';
import { UpdateSubmissionDto } from './dto/update-submission.dto';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { FindOneId } from '../common/validators/param.validator';
import { UsersService } from 'src/users/users.service';
import { ExercisesService } from 'src/exercises/exercises.service';
import { PostSubmissionDto } from './dto/post-submission.dto';

@Controller('submissions')
export class SubmissionController {
  constructor(
    private readonly submissionsService: SubmissionsService,
    private readonly usersService: UsersService,
    private readonly exercisesService: ExercisesService,
  ) {}

  @Post()
  @UsePipes(new ValidationPipe({ transform: true }))
  async create(
    @Body() createSubmissionDto: CreateSubmissionDto,
  ): Promise<PostSubmissionDto> {
    const exercise = await this.exercisesService.findOne(
      createSubmissionDto.exerciseId,
      true,
    );

    if (!(await this.usersService.findOne(createSubmissionDto.userId))) {
      throw new HttpException("User id doesn't exists", HttpStatus.NOT_FOUND);
    } else if (!exercise) {
      throw new HttpException(
        "Exercise id doesn't exists",
        HttpStatus.NOT_FOUND,
      );
    }
    return this.submissionsService.create(
      createSubmissionDto,
      exercise.solution,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id/') //exerciseId
  async findOneForCurrentExercise(@Param() params: FindOneId, @Request() req) {
    const submission =
      await this.submissionsService.findOneByUserIdAndExerciseId(
        req.user.id,
        +params.id,
      );
    if (submission) {
      return submission;
    } else {
      throw new HttpException(
        'Submission not found with exerciseId',
        HttpStatus.NOT_FOUND,
      );
    }
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateSubmissionDto: UpdateSubmissionDto,
  ) {
    return this.submissionsService.update(+id, updateSubmissionDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    if (await this.submissionsService.remove(+id)) {
      throw new HttpException('', HttpStatus.NO_CONTENT);
    } else return false;
  }
}

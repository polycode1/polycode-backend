import { Module } from '@nestjs/common';
import { SubmissionsService } from './submissions.service';
import { SubmissionController } from './submissions.controller';
import { DatabaseModule } from 'src/database/database.module';
import { submissionsProviders } from './submissions.providers';
import { UsersModule } from 'src/users/users.module';
import { ExercisesModule } from 'src/exercises/exercises.module';

@Module({
  imports: [DatabaseModule, UsersModule, ExercisesModule],
  controllers: [SubmissionController],
  providers: [SubmissionsService, ...submissionsProviders],
})
export class SubmissionModule {}

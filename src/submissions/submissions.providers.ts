import { SUBMISSIONS_REPOSITORY } from 'src/constants';
import { Submission } from './entities/submission.entity';

export const submissionsProviders = [
  {
    provide: SUBMISSIONS_REPOSITORY,
    useValue: Submission,
  },
];

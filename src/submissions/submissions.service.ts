import { Inject, Injectable } from '@nestjs/common';
import { ExerciseStatus, SUBMISSIONS_REPOSITORY } from 'src/constants';
import { CreateSubmissionDto } from './dto/create-submission.dto';
import { PostSubmissionDto } from './dto/post-submission.dto';
import { UpdateSubmissionDto } from './dto/update-submission.dto';
import { Submission } from './entities/submission.entity';
import axios from 'axios';
@Injectable()
export class SubmissionsService {
  constructor(
    @Inject(SUBMISSIONS_REPOSITORY)
    private submissionsRepository: typeof Submission,
  ) {}

  async create(
    createSubmissionDto: CreateSubmissionDto,
    solution: string,
  ): Promise<PostSubmissionDto> {
    const { language, code } = createSubmissionDto;
    const submissionReturn = await axios.post(process.env.RUNNER_API_URL, {
      language,
      code,
    });
    const status =
      submissionReturn.data.stdout === solution
        ? ExerciseStatus.FINISHED
        : ExerciseStatus.STARTED;

    await this.submissionsRepository.upsert<Submission>({
      ...createSubmissionDto,
    });

    return {
      statusCode: submissionReturn.data.statusCode,
      stdout: submissionReturn.data.stdout,
      stderr: submissionReturn.data.stderr,
      status,
    };
  }

  findAll() {
    return this.submissionsRepository.findAll<Submission>();
  }

  findOne(id: number) {
    return this.submissionsRepository.findOne<Submission>({
      where: { id: id },
      raw: true,
    });
  }

  findOneByUserIdAndExerciseId(userId: number, exerciseId: number) {
    return this.submissionsRepository.findOne<Submission>({
      where: { userId: userId, exerciseId: exerciseId },
      raw: true,
    });
  }

  update(id: number, updateSubmissionDto: UpdateSubmissionDto) {
    return this.submissionsRepository.update<Submission>(
      { ...updateSubmissionDto },
      { where: { id: id }, returning: true },
    );
  }

  remove(id: number) {
    return this.submissionsRepository.destroy({ where: { id: id } });
  }
}

import { IsNotEmpty } from 'class-validator';

export class CreateTokenDto {
  @IsNotEmpty()
  value: string;
  @IsNotEmpty()
  userId: number;
}

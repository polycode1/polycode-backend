import { Column, ForeignKey, Model, Table } from 'sequelize-typescript';
import { User } from 'src/users/entities/user.entity';

@Table
export class Token extends Model {
  @Column
  value: string;

  @ForeignKey(() => User)
  @Column
  userId: number;
}

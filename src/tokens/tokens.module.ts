import { Module } from '@nestjs/common';
import { TokensService } from './tokens.service';
import { TokensController } from './tokens.controller';
import { DatabaseModule } from 'src/database/database.module';
import { tokensProviders } from './tokens.providers';

@Module({
  imports: [DatabaseModule],
  controllers: [TokensController],
  providers: [TokensService, ...tokensProviders],
  exports: [TokensService],
})
export class TokensModule {}

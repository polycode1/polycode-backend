import { TOKENS_REPOSITORY } from 'src/constants';
import { Token } from './entities/token.entities';

export const tokensProviders = [
  {
    provide: TOKENS_REPOSITORY,
    useValue: Token,
  },
];

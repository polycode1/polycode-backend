import { Inject, Injectable } from '@nestjs/common';
import { TOKENS_REPOSITORY } from 'src/constants';
import { Token } from './entities/token.entities';

@Injectable()
export class TokensService {
  constructor(
    @Inject(TOKENS_REPOSITORY)
    private tokensRepository: typeof Token,
  ) {}

  create(access_token: string, userId: number): Promise<Token> {
    return this.tokensRepository.create<Token>({
      value: access_token,
      userId: userId,
    });
  }

  remove(userId: number) {
    return this.tokensRepository.destroy({ where: { userId: userId } });
  }

  findOne(userId: number): Promise<Token> {
    return this.tokensRepository.findOne<Token>({
      where: { userId: userId },
      raw: true,
    });
  }
}

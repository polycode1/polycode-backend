import { IsEmail, MinLength } from 'class-validator';

export class BaseUserDto {
  @IsEmail()
  email: string;

  @MinLength(3)
  username: string;
}

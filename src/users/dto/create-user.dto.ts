import { MinLength } from 'class-validator';
import { BaseUserDto } from './base-user.dto';
export class CreateUserDto extends BaseUserDto {
  @MinLength(6)
  password: string;
}

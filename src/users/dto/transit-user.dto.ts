import { IsNotEmpty, IsNumber } from 'class-validator';
import { BaseUserDto } from './base-user.dto';

export class TransitUserDto extends BaseUserDto {
  @IsNumber()
  id: number;

  @IsNotEmpty()
  token: string;
}

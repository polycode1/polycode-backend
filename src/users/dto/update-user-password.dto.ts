import { MinLength } from 'class-validator';

export class UpdateUserPasswordDto {
  @MinLength(8)
  oldPassword: string;
  @MinLength(8)
  newPassword: string;
}

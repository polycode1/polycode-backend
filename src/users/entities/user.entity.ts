import {
  Table,
  Column,
  Model,
  Index,
  Default,
  HasOne,
  HasMany,
} from 'sequelize-typescript';
import { IsEmail, MinLength } from 'class-validator';
import { Submission } from 'src/submissions/entities/submission.entity';
import { Validation } from 'src/validations/entities/validation.entity';

@Table
export class User extends Model {
  @Index({ unique: true })
  @IsEmail()
  @Column
  email: string;

  @MinLength(3)
  @Column
  username: string;

  @MinLength(6)
  @Column
  password: string;

  @Default(false)
  @Column
  verified: boolean;

  @HasMany(() => Submission)
  submissions: Submission[];

  @HasOne(() => Validation, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  validation: Validation;
}

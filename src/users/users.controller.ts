import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpException,
  HttpStatus,
  UsePipes,
  ValidationPipe,
  UseGuards,
  Request,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { FindOneId, FindOneUuid } from '../common/validators/param.validator';
import { TokensService } from 'src/tokens/tokens.service';
import { BaseUserDto } from './dto/base-user.dto';
import { LocalAuthGuard } from 'src/auth/guards/local-auth.guard';
import { AuthService } from '../auth/auth.service';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { UpdateUserPasswordDto } from './dto/update-user-password.dto';

@Controller('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly tokensService: TokensService,
    private readonly authService: AuthService,
  ) {}

  @Post()
  @UsePipes(new ValidationPipe({ transform: true }))
  async create(@Body() createUserDto: CreateUserDto): Promise<BaseUserDto> {
    if (await this.usersService.findOneByEmail(createUserDto.email)) {
      throw new HttpException('Email already exists', HttpStatus.CONFLICT);
    } else {
      const response = await this.usersService.create(createUserDto);
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { password, ...user } = response;
      return user;
    }
  }

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @Get()
  findAll() {
    return this.usersService.findAll();
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param() params: FindOneId, @Request() req) {
    if (req.user.id !== +params.id) {
      throw new HttpException(
        `You don't have the right to access this userId`,
        HttpStatus.FORBIDDEN,
      );
    }
    return req.user;
    return this.usersService.findOne(+params.id);
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  async update(
    @Param() params: FindOneId,
    @Request() req,
    @Body() updateUserDto: UpdateUserDto | UpdateUserPasswordDto,
  ) {
    if (req.user.id !== +params.id) {
      throw new HttpException(
        `You don't have the right to access this user`,
        HttpStatus.FORBIDDEN,
      );
    } else if ((updateUserDto as UpdateUserPasswordDto).oldPassword) {
      const response = await this.usersService.updatePassword(
        req.user.id,
        updateUserDto as UpdateUserPasswordDto,
      );
      if (response) {
        return response;
      } else {
        throw new HttpException('', HttpStatus.NOT_FOUND);
      }
    } else {
      return this.usersService.update(
        req.user.id,
        updateUserDto as UpdateUserDto,
      );
    }
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param() params: FindOneId) {
    if (this.usersService.remove(+params.id)) {
      throw new HttpException('', HttpStatus.NO_CONTENT);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('logout')
  logout(@Request() req) {
    this.tokensService.remove(req.user.id);
    throw new HttpException('', HttpStatus.NO_CONTENT);
  }

  @Post('verify/:uuid')
  async verify(@Param() params: FindOneUuid) {
    const isVerified = await this.usersService.verify(params.uuid);
    //TODO verify if the user is already verified
    if (isVerified) {
      throw new HttpException('', HttpStatus.NO_CONTENT);
    } else {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }
  }
}

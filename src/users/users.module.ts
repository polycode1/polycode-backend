import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { usersProviders } from './users.providers';
import { DatabaseModule } from '../database/database.module';
import { MailModule } from 'src/mail/mail.module';
import { ValidationsModule } from 'src/validations/validations.module';
import { TokensModule } from 'src/tokens/tokens.module';

@Module({
  imports: [DatabaseModule, ValidationsModule, MailModule, TokensModule],
  controllers: [UsersController],
  providers: [UsersService, ...usersProviders],
  exports: [UsersService],
})
export class UsersModule {}

import { Inject, Injectable } from '@nestjs/common';
import * as bcrypt from 'bcryptjs';
import { MailService } from 'src/mail/mail.service';
import { USERS_REPOSITORY } from '../constants';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { v4 as uuidv4 } from 'uuid';
import { ValidationsService } from 'src/validations/validations.service';
import { UpdateUserPasswordDto } from './dto/update-user-password.dto';

@Injectable()
export class UsersService {
  constructor(
    @Inject(USERS_REPOSITORY)
    private readonly usersRepository: typeof User,
    private readonly mailService: MailService,
    private readonly validationsService: ValidationsService,
  ) {}

  private hashPassword(password) {
    return bcrypt.hash(password, 10);
  }

  comparePassword(password: string, hash: string): boolean {
    return bcrypt.compare(password, hash);
  }

  async create(createUserDto: CreateUserDto): Promise<User> {
    createUserDto.password = await this.hashPassword(createUserDto.password);

    const user = await User.create<User>(
      {
        ...createUserDto,
      },
      { raw: true },
    );
    const uuid = uuidv4();

    this.mailService.sendUserConfirmation(createUserDto as User, uuid);
    this.validationsService.create({ uuid: uuid, userId: user.id });

    return user.toJSON();
  }

  findAll(): Promise<User[]> {
    return this.usersRepository.findAll<User>();
  }

  findOne(id: number, excludePassword = true): Promise<User> {
    return this.usersRepository.findOne<User>({
      where: {
        id,
      },
      attributes: {
        exclude: excludePassword ? ['password'] : [],
      },
      raw: true,
    });
  }

  async findOneByEmail(email: string): Promise<User> {
    return this.usersRepository.findOne<User>({
      where: {
        email: email,
      },
      raw: true,
    });
  }

  async findOneByUsername(username: string): Promise<User> {
    return this.usersRepository.findOne<User>({
      where: {
        username: username,
      },
      attributes: {
        exclude: ['password'],
      },
      raw: true,
    });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const response = await this.usersRepository.update(updateUserDto, {
      where: {
        id,
      },
      returning: true,
    });
    if (response[0] === 1) {
      const { email, username, verified } = response[1][0].toJSON();
      return { id, email, username, verified };
    } else {
      return 0;
    }
  }

  async updatePassword(id: number, updateUserDto: UpdateUserPasswordDto) {
    const user = await this.findOne(id, false);

    const arePasswordEqual = await this.comparePassword(
      updateUserDto.oldPassword,
      user.password,
    );

    const newPassword = await this.hashPassword(updateUserDto.newPassword);

    if (arePasswordEqual) {
      const response = await this.usersRepository.update(
        { password: newPassword },
        {
          where: {
            id,
          },
          returning: true,
        },
      );
      if (response[0] === 1) {
        const { email, username, verified } = response[1][0].toJSON();
        return { id, email, username, verified };
      }
    } else {
      console.log('passwords dont match');
      return null;
    }
  }

  async remove(id: number) {
    return 1 === (await this.usersRepository.destroy({ where: { id } }));
  }

  async verify(uuidUser: string): Promise<boolean> {
    const { uuid, userId } = await this.validationsService.findOne(uuidUser);
    if (userId && uuid) {
      await this.usersRepository.update(
        { verified: true },
        { where: { id: userId } },
      );
      return true;
    } else {
      return false;
    }
  }
}

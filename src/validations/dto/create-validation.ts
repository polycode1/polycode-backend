import { IsNotEmpty, IsNumber, IsUUID } from 'class-validator';

export class CreateValidationDto {
  @IsUUID()
  uuid: string;

  @IsNumber()
  @IsNotEmpty()
  userId: string;
}

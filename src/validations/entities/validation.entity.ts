import {
  BelongsTo,
  Column,
  ForeignKey,
  Index,
  IsUUID,
  Model,
  Table,
} from 'sequelize-typescript';
import { User } from 'src/users/entities/user.entity';

@Table
export class Validation extends Model {
  @Index({ unique: true })
  @IsUUID(4)
  @Column({ primaryKey: true })
  uuid: string;

  @ForeignKey(() => User)
  userId: number;

  @BelongsTo(() => User)
  user: User;
}

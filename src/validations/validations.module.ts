import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/database/database.module';
import { validationsProviders } from './validations.providers';
import { ValidationsService } from './validations.service';

@Module({
  imports: [DatabaseModule],
  providers: [ValidationsService, ...validationsProviders],
  exports: [ValidationsService, ...validationsProviders],
})
export class ValidationsModule {}

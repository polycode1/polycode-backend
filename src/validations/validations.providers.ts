import { VALIDATIONS_REPOSITORY } from '../constants';
import { Validation } from './entities/validation.entity';

export const validationsProviders = [
  {
    provide: VALIDATIONS_REPOSITORY,
    useValue: Validation,
  },
];

import { Inject, Injectable } from '@nestjs/common';
import { VALIDATIONS_REPOSITORY } from 'src/constants';
import { CreateValidationDto } from './dto/create-validation';
import { Validation } from './entities/validation.entity';

@Injectable()
export class ValidationsService {
  constructor(
    @Inject(VALIDATIONS_REPOSITORY)
    private readonly validationsRepository: typeof Validation,
  ) {}

  async create(createValidationDto: CreateValidationDto): Promise<Validation> {
    return (
      await this.validationsRepository.create<Validation>(
        { ...createValidationDto },
        { raw: true },
      )
    ).toJSON();
  }

  async findOne(uuid: string): Promise<Validation> {
    return this.validationsRepository.findOne<Validation>({
      where: {
        uuid: uuid,
      },
      raw: true,
    });
  }

  async remove(userId: number): Promise<boolean> {
    return (
      1 ===
      (await this.validationsRepository.destroy({
        where: {
          userId: userId,
        },
      }))
    );
  }
}
